from urllib.request import HTTPPasswordMgrWithDefaultRealm
import requests
import json
import sys
from flask import Flask, request, Response, abort, render_template, redirect, url_for, send_file
from flask_login import LoginManager, login_user, logout_user, login_required, UserMixin, current_user
from models.user import User
from utils.db import DB
from modules.LoginManager import LoginManagerUser



# Creating a Flask object and setting the template folder to templates.


app = Flask(__name__, template_folder='templates')
login_manager = LoginManager()
login_manager.init_app(app)
app.config['SECRET_KEY'] = 'myAppSecretDahian'


f = open("config.json")
app.configs2 = json.load(f)
app.version = "0.0.1"
f.close()


@login_manager.user_loader
def load_user(id):
    """
    It takes the id of a user, queries the database for the user with that id, and returns a User object
    with the user's information

    :param id: The user ID to load
    :return: The user object is being returned.
    """
    res = db.query(
        'SELECT * FROM public."users" where id = \'' + str(id) + '\'')
    try:
        return User(res[0])
    except:
        return None


@login_manager.unauthorized_handler
def unauthorized_callback():
    """
    If the user is not logged in, redirect them to the login page
    :return: A redirect to the login page.
    """
    return redirect(url_for('login'))


@app.errorhandler(404)
def page_not_found(e):
    # Returning a 404 error page.
    return render_template('custompages/404.html'), 404


@app.errorhandler(500)
def page_not_found(e):
    # Returning a 500 error page.
    return render_template('custompages/500.html'), 500


@app.errorhandler(401)
def page_not_authorized(e):
    # Returning a 401 error page.
    return render_template('custompages/401.html'), 401


db = DB()

LoginManagerUser(app, db)


