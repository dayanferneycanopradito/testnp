from flask_login import UserMixin

class User(UserMixin):
    def __init__(self, data):
        """
        The function __init__() is a special function in Python classes. It is run as soon as an object
        of a class is instantiated. The method is useful to do any initialization you want to do with
        your object
        
        :param data: The data that will be used to create the node
        """
        self.id = data[0]
        self.data = data