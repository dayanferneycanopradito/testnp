import psycopg2
from psycopg2.extras import RealDictCursor
import json
import os


class DB():

    def __init__(self):
        """
        It opens a file called config.json, reads the contents of the file, and then uses the contents
        of the file to connect to a database
        """
        f = open("config.json")
        self.configs = json.load(f)
        f.close()

#--        self.conn = psycopg2.connect(
#           "host={} port={} dbname={} user={} password={}". format(self.configs['db_host'], self.configs['db_port'], #self.configs['db_name'], self.configs['db_user'], self.configs['db_pass']))
#        self.conn.autocommit = True
#        self.conn.set_client_encoding('UTF8')#

    def query(self, querystr):
        """
        It takes a string as an argument, prints it, and then executes it as a SQL query
        
        :param querystr: The query string to be executed
        :return: A list of tuples.
        """
        print(querystr)

        cur = self.conn.cursor()
      
        cur.execute(querystr)
      
        return cur.fetchall()

    def queryJson(self, querystr):
        """
        It takes a query string, executes it, and returns the results as a list of dictionaries
        
        :param querystr: The query string to be executed
        :return: A list of dictionaries.
        """
        print(querystr)

        cur = self.conn.cursor(cursor_factory=RealDictCursor)
        cur.execute(querystr)
        return cur.fetchall()

    def query2(self, querystr):
        """
        It takes a string as an argument, prints it, and then executes it as a SQL query
        
        :param querystr: the query string
        """
        print(querystr)

        cur = self.conn.cursor()
        
        cur.execute(querystr)
