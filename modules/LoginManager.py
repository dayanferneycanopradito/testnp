from flask import Flask, request, Response, abort, render_template, redirect, url_for, send_file
from flask_login import LoginManager, login_user, logout_user, login_required, UserMixin, current_user
from models.user import User
from modules.MainModule import MainModule


class LoginManagerUser(MainModule):

    def __init__(self, app, db):

        super().__init__(app, db)

        @app.route('/logout')
        @login_required
        def logout():
       
            logout_user()
            return redirect(url_for('login'))

        
        @app.route('/',methods=['GET'])
        def init_app():
            return redirect(url_for('login'))

        @app.route('/login', methods=['GET', 'POST'])
        def login():
          
            if request.method == 'POST':
                username = request.form.get('username', None)
                password = request.form.get('password', None)

                res = db.query(
                    """
                        SELECT * FROM public."users" where username = %s and password = md5(md5(%s))')
                        """,(username,password, )
                )

                if len(res) == 0:
                    return redirect(url_for('login'))

                login_user(User(res[0]))

                return redirect(url_for('home'))

            else:
                return render_template('users/login.html', version=app.version)

        @app.route('/register', methods=['GET', 'POST'])
        def register():
          
            if request.method == 'POST':
                
                name = request.form.get('name', None)
                username = request.form.get('username', None)
                email = request.form.get('email', None)
                userpassword = request.form.get('userpassword', None)
                
                return redirect(url_for('login'))

            else:
                return render_template('users/register.html')